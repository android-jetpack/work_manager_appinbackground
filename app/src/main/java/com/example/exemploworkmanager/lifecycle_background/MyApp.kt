package com.example.exemploworkmanager.lifecycle_background

import android.app.Activity
import android.app.Application
import android.os.Bundle

class MyApp: Application() {


    private val lifecycleCallbacks = LifecycleCallback()
    val inForenground = lifecycleCallbacks.activitiesStarted



    override fun onCreate() {
        super.onCreate()
        registerActivityLifecycleCallbacks(lifecycleCallbacks)
    }


    inner class LifecycleCallback : ActivityLifecycleCallbacks{
        var activitiesStarted = 0

        override fun onActivityPaused(activity: Activity?) {}
        override fun onActivityResumed(activity: Activity?) {}
        override fun onActivityStarted(activity: Activity?) {
            activitiesStarted++
        }
        override fun onActivityDestroyed(activity: Activity?) {}
        override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {}
        override fun onActivityStopped(activity: Activity?) {
            activitiesStarted--
        }
        override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {}
    }
}