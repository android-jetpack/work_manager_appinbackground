package com.example.exemploworkmanager.lifecycle_background

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.exemploworkmanager.R

class TesteLifecycleActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_teste_lifecycle)

        val app = application as MyApp
        if (app.inForenground == 0){
            Log.i("BACKGROUND","app indo para background")
        }
    }
}