package com.example.exemploworkmanager.wok_manager

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.work.*
import com.example.exemploworkmanager.R
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }



    fun btStart(v: View){
        //defini restrições:
        val restricoesConstraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.NOT_REQUIRED)
            .setRequiresBatteryNotLow(false)
            .setRequiresCharging(false)
            .setRequiresStorageNotLow(false)
            .build()


        // instancia o workManager
        val inputData = Data.Builder().putString("name","unica").putInt("id",0).build()
        val mWorkManager = WorkManager.getInstance()
        val dayWorkBuilder = PeriodicWorkRequest.Builder(MyWorker::class.java, 15, TimeUnit.MINUTES, 5, TimeUnit.MINUTES)
            .addTag("work")
            .setInputData(inputData)
            .setConstraints(restricoesConstraints)
            .build()
        mWorkManager.enqueue(dayWorkBuilder)
    }



    //empilha workManager passando dados e executa sincronamente.(um apos o termino do outro)
    private fun stackWorkManage(){
        val mWorkManager = WorkManager.getInstance()

        val input1 = Data.Builder().putString("name","primeiro").putInt("id",1).build()
        val work1 = OneTimeWorkRequest.Builder(MyWorker::class.java).setInputData(input1).build()

        val input2 = Data.Builder().putString("name","segundo").putInt("id",2).build()
        val work2 = OneTimeWorkRequest.Builder(MyWorker::class.java).setInputData(input2).build()

        val input3 = Data.Builder().putString("name","terceiro").putInt("id",3).build()
        val work3 = OneTimeWorkRequest.Builder(MyWorker::class.java).setInputData(input3).build()

        mWorkManager
            .beginWith(listOf(work1, work2))
            .then(work3)
            .enqueue()
    }








    fun btCancel(v: View){
        val mWorkManager = WorkManager.getInstance()
        mWorkManager.cancelAllWork()
    }
}
