package com.example.exemploworkmanager.wok_manager

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.exemploworkmanager.R
import java.text.SimpleDateFormat
import java.util.*

class MyWorker(var context: Context, var workerParams: WorkerParameters) : Worker(context, workerParams) {


    override fun doWork(): Result {
        setNotification(inputData.getInt("id",-1), inputData.getString("name"))
        Log.i("WORK","entrou doWok()")

        //pode retornar dados aqui no success() ou failure()
        return Result.success()
    }



    private fun setNotification(id: Int, name: String?){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val df = SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault())
            val NOTIFICATION_CHANNEL_ID = "com.work.notifi"+id
            val channelName = "Notificação WorkManager"+id
            val chan = NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_HIGH)
            chan.lightColor = Color.BLUE
            val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            val notificationBuilder = NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
            val notification = notificationBuilder
                .setSmallIcon(android.R.drawable.ic_delete)
                .setDefaults(Notification.DEFAULT_ALL)
                .setColor(ContextCompat.getColor(context,
                    R.color.colorPrimaryDark
                ))
                .setContentTitle("Notificação WorkManager")
                .setContentText("Disparou: ${df.format(Date())} - $name")
                .setAutoCancel(true)
                .build()
            manager.createNotificationChannel(chan)
            manager.notify(id, notification)
        }
    }
}